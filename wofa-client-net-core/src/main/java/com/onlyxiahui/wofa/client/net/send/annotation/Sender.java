
package com.onlyxiahui.wofa.client.net.send.annotation;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description <br>
 * Date 2021-03-13 20:03:53<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Target(TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Sender {

}
