package com.onlyxiahui.wofa.client.work.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.onlyxiahui.app.context.AbstractMaterial;
import com.onlyxiahui.app.context.AppContext;
import com.onlyxiahui.framework.net.handler.NetHandler;
import com.onlyxiahui.framework.net.handler.connect.Connector;
import com.onlyxiahui.framework.net.handler.connect.action.ConnectBackAction;
import com.onlyxiahui.framework.net.handler.connect.action.ConnectStatusAction;
import com.onlyxiahui.framework.net.handler.connect.data.ConnectData;
import com.onlyxiahui.framework.net.handler.connect.type.IdleStatus;
import com.onlyxiahui.framework.net.handler.data.handler.ReceiveHandler;
import com.onlyxiahui.wofa.client.net.action.DispatcherPower;
import com.onlyxiahui.wofa.client.net.core.connect.SocketConnector;
import com.onlyxiahui.wofa.client.work.core.impl.ReceiveHandlerImpl;

/**
 * Description <br>
 * Date 2020-11-15 13:24:46<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class WorkContext extends AbstractMaterial {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private Connector connector;// = new SocketConnector();// new WebSocketConnector();
	private final NetHandler netHandler = new NetHandler();;

	public WorkContext(AppContext appContext) {
		super(appContext);
		initialize();
	}

	private void initialize() {
		netHandler.addConnectStatusAction(new ConnectStatusAction() {

			@Override
			public void onStatusChange(boolean isConnected) {
				if (logger.isTraceEnabled()) {
					logger.trace("isConnected：" + isConnected);
				}
			}

			@Override
			public void onIdle(IdleStatus idleStatus) {
				if (logger.isTraceEnabled()) {
					logger.trace("idleStatus：" + idleStatus);
				}
			}

			@Override
			public void onAfterAutoConnect(boolean connected) {
				if (logger.isTraceEnabled()) {
					logger.trace("onAfterAutoConnect:" + connected);
				}
			}
		});

//		netHandler.addReceiveHandler(new ReceiveHandler() {
//
//			@Override
//			public void receive(Object data, Map<String, HandleData> handleDataMap) {
//			}
//		});
//		netHandler.getDataHandler().addDataSendAction(new DataSendAction() {
//
//			@Override
//			public boolean send(Object data) {
//				return true;
//			}
//		});
		ReceiveHandler rh = appContext.getObject(ReceiveHandlerImpl.class);
		netHandler.addReceiveHandler(rh);

		this.setConnector(new SocketConnector());
	}

	public void connect(ConnectData connectData, ConnectBackAction action) {
		netHandler.connect(connectData, action);
	}

	public Connector getConnector() {
		return connector;
	}

	public void setConnector(Connector connector) {
		if (null != this.connector) {
			this.connector.close();
		}
		this.connector = connector;
		netHandler.setConnector(connector);
	}

	public NetHandler getNetHandler() {
		return netHandler;
	}

	public void closeConnect() {
		netHandler.closeConnect();
	}

	public void setActionPackages(String... path) {
		DispatcherPower dc = appContext.getObject(DispatcherPower.class);
		dc.getActionDispatcher().scan(path);
	}
}
